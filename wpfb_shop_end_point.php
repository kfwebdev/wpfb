<?php

/**
 * This class contains the custom post type meta methods.
 */

class WPFB_Tea_Shops_End_Point {
    private $wpdb;

    function __construct() {
        global $wpdb;

        $this->register_tea_shops_end_point();
        $this->tea_shops_table = $wpdb->prefix . 'wpfb_shops';
        $this->wpdb = $wpdb;
    }

    public function register_tea_shops_end_point() {
        register_rest_route(
            'wpfb/v1',
            '/shops',
            array(
                'method' => 'GET',
                'callback' => array( $this, 'wpfb_get_tea_shops_end_point' ),
                'args' => array(
                )
                // ,
                // 'permission_callback' => function() {
                //     return is_user_logged_in();
                // }
            )
        );

        register_rest_route(
            'wpfb/v1',
            '/shops/(?P<id>\d+)',
            array(
                'method' => 'GET',
                'callback' => array( $this, 'wpfb_get_tea_shops_end_point' ),
                'args' => array(
                    'id' => array(
                        'validate_callback' => function($param) {
                            return is_numeric( $param );
                        }
                    )
                )
                // ,
                // 'permission_callback' => function() {
                //     return is_user_logged_in();
                // }
            )
        );

    }

    public function wpfb_get_tea_shops_end_point( WP_REST_Request $request ) {
        if ( !empty( $request->get_param( 'id' ) ) ) {
            $shop_id = $request->get_param( 'id' );
            $sql = $this->wpdb->prepare( "SELECT * FROM $this->tea_shops_table WHERE `id` = %d ORDER BY `updated` DESC", $shop_id );
        } else {
            $sql = "SELECT * FROM $this->tea_shops_table ORDER BY `updated` DESC";
        }
        $results = $this->wpdb->get_results( $sql , ARRAY_A );
        if ( empty( $results ) ) {
            return null;
        }

        return $results;
    }
}

new WPFB_Tea_Shops_End_Point;

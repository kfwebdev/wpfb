<?php

/**
 * This class contains the custom post type meta methods.
 */

class WPFB_Shop_Search {
    function __construct() {
        $this->register_shop_search_query();
    }

    private function register_shop_search_query() {
        add_action( 'pre_get_posts', array( $this, 'exclude_category' ) );
    }

    function exclude_category( $query ) {
        // $meta_query = $query->get('meta_query');

		// $location_json = urldecode( stripslashes( $_GET['wpfbLocation'] ) );
		// $location = json_decode($location_json);
		// $location->accuracy = filter_var( $location->accuracy, FILTER_SANITIZE_NUMBER_INT );
		// $location->latitude = filter_var( $location->latitude, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
        // $location->longitude = filter_var( $location->longitude, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );

        // exit();
    }
}

new WPFB_Shop_Search();

<?php

/**
 * This class contains the custom post type methods.
 */

class WPFB_Shop_Post_Type {

    const WPFB_PREFIX = 'wpfb_';
    const SHOP_POST_TYPE = self::WPFB_PREFIX . 'shop';
    const SHOP_ADDRESS = 'shop_address';
    const SHOP_LATITUDE = 'shop_latitude';
    const SHOP_LONGITUDE = 'shop_longitude';

    function __construct() {
        $this->register_shop();
        add_action( 'wp_insert_post', array( $this, 'wpfb_add_shop_meta_fields' ), 10, 4 );
        add_action( 'save_post', array( $this, 'wpfb_after_save_post' ), 10, 4 );
    }

    private function register_shop() {
        $args = array(
            'label'              => __( 'Shops' ),
            'description'        => __( 'Shop' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 's' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'show_in_rest'       => true,
            'rest_base'          => self::SHOP_POST_TYPE,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'comments', 'page-attributes' )
        );

        register_post_type( self::SHOP_POST_TYPE, $args );
    }

    function wpfb_add_shop_meta_fields( $post_id, $post, $update ) {
        // If this is a revision, don't add shop meta fields
        if ( $post->post_type != self::SHOP_POST_TYPE || wp_is_post_revision( $post_id ) )
            return;
        // Add address, latitude and longitude meta fields
        add_post_meta( $post_id, self::SHOP_ADDRESS, '', true );
        add_post_meta( $post_id, self::SHOP_LATITUDE, '', true );
        add_post_meta( $post_id, self::SHOP_LONGITUDE, '', true );
    }

    function wpfb_after_save_post( $post_id, $post, $update ) {
        // If post_type is wpfb shop, update empty gps data from address
        if ( $post->post_type == self::SHOP_POST_TYPE ) {
            $post_meta = get_post_meta($post_id);
            $shop_address = $post_meta[self::SHOP_ADDRESS][0];
            $shop_latitude = $post_meta[self::SHOP_LATITUDE][0];
            $shop_longitude = $post_meta[self::SHOP_LONGITUDE][0];

            if ( !empty($shop_address) && ( empty($shop_latitude) || empty($shop_longitude) ) ) {
                $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='. urlencode($shop_address) .'&sensor=false');
                $result = json_decode($geocode);
                $latitude = $result->results[0]->geometry->location->lat;
                $longitude = $result->results[0]->geometry->location->lng;
                if (!empty($latitude)) {
                    update_post_meta( $post_id, self::SHOP_LATITUDE, $latitude );
                }
                if (!empty($longitude)) {
                    update_post_meta( $post_id, self::SHOP_LONGITUDE, $longitude );
                }
            }
        }
    }
}

new WPFB_Shop_Post_Type();

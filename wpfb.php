<?php

/*
  Plugin Name: wpfb
  Plugin URI: https://bitbucket.org/kfwebdev/wpfb
  Description: Finding Boba Custom Post Type
  Version: 1.0.1
  Author: Finding Boba
  Author URI: http://findingboba.com
  License: GPL V3
  Bitbucket Plugin URI: kfwebdev/wpfb
 */

class WPFB {
	private static $instance = null;
	private $plugin_path;
	private $plugin_url;
	private $plugin_version = '1.0.0';
    private $text_domain = '';

	public static $rest_namespace = 'wpfb/v1';

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function get_instance() {
		// If an instance hasn't been created and set to $instance create an instance and set it to $instance.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
	 */
	private function __construct() {
		$this->plugin_path = plugin_dir_path( __FILE__ );
		$this->plugin_url  = plugin_dir_url( __FILE__ );

		load_plugin_textdomain( $this->text_domain, false, $this->plugin_path . '\lang' );

		add_action( 'init', array( $this, 'initialization' ) );
        add_action( 'rest_api_init', array( $this, 'rest_api_initialization' ) );

		add_action( 'admin_enqueue_scripts', array( $this, 'register_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_styles' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_styles' ) );

		register_activation_hook( __FILE__, array( $this, 'activation' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivation' ) );

		$this->run_plugin();
	}

	public function get_plugin_url() {
		return $this->plugin_url;
	}

	public function get_plugin_path() {
		return $this->plugin_path;
	}

    /**
     * Place code that runs at WordPress initialization here.
     */
    public function initialization() {
		// Tea shop custom post type
		require_once( plugin_dir_path( __FILE__ ) . 'wpfb_drink_post_type.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'wpfb_shop_post_type.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'wpfb_shop_search.php' );
	}

    /**
     * Place code that runs at WordPress REST API initialization here.
     */
    public function rest_api_initialization() {
		// Tea shop meta
		require_once( plugin_dir_path( __FILE__ ) . 'wpfb_shop_meta.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'wpfb_shop_end_point.php' );
	}

    /**
     * Place code that runs at plugin activation here.
     */
    public function activation() {

	}

    /**
     * Place code that runs at plugin deactivation here.
     */
    public function deactivation() {

	}

    /**
     * Enqueue and register JavaScript files here.
     */
    public function register_scripts() {

	}

    /**
     * Enqueue and register CSS files here.
     */
    public function register_styles() {

	}

    /**
     * Place code for your plugin's functionality here.
     */
    private function run_plugin() {

	}
}

WPFB::get_instance();

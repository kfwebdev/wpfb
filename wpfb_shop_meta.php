<?php

/**
 * This class contains the custom post type meta methods.
 */

class WPFB_Shop_Meta {
    function __construct() {
        $this->register_get_shop_meta();
    }

    private function register_get_shop_meta() {
        register_rest_field( 'wpfb_shop',
            'wpfb_shop_meta',
            array(
                'get_callback'    => 'wpfb_get_shop_meta',
                'update_callback' => null,
                'schema'          => null,
            )
        );

        function wpfb_get_shop_meta( $object, $field_name, $request ) {
            $shop_address = get_post_meta( $object[ 'id' ], 'shop_address', true );
            $shop_latitude = get_post_meta( $object[ 'id' ], 'shop_latitude', true );
            $shop_longitude = get_post_meta( $object[ 'id' ], 'shop_longitude', true );
            return [ $shop_address, $shop_latitude, $shop_longitude ];
        }
    }
}

new WPFB_Shop_Meta();

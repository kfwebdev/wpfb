if (window.jQuery && !window.$) { window.$ = jQuery; }
if (!window.wpfb) { 
    window.wpfb = {
        isFindingLocation: false,
        isSearchSubmitted: false
    };
    wpfb = wpfb;
}

wpfb.geoSuccess = function(e) {
    console.log('success', e);
    const location = {
        accuracy: e.coords.accuracy,
        latitude: e.coords.latitude,
        longitude: e.coords.longitude,
    };

    $(wpfb.searchForm).find('.wpfbLocation').val(JSON.stringify(location));
    $(wpfb.searchForm).find('#s').val('');
    $(wpfb.searchForm).find('.et_pb_s').val('');
    wpfb.isFindingLocation = false;
    if (wpfb.isSearchSubmitted) {
        $(wpfb.searchForm).submit();
    }
};

wpfb.geoError = function(e) {
    console.log('error', e);
    wpfb.isSearchSubmitted = false;
    wpfb.isFindingLocation = false;
    $(wpfb.searchForm).find('input[type=submit]').removeAttr('disabled');
};

wpfb.geoProgress = function(e) {
    console.log('progress', e);
};

wpfb.submitSearch = function(e) {
    wpfb.isSearchSubmitted = true;
    wpfb.searchForm = e.currentTarget;
    $(wpfb.searchForm).find('input[type=submit]').attr('disabled', 'disabled');
    if (wpfb.isFindingLocation) {
        e.preventDefault();
        return false; 
    }
};

(function($) {
    $('.searchform, .et_pb_searchform').on('submit', wpfb.submitSearch);
    $('.searchform #s, .et_pb_searchform .et_pb_s').after('<input type="hidden" name="wpfbLocation" class="wpfbLocation" value="" />');
    $('.searchform #s, .et_pb_searchform .et_pb_s')
        .attr('data-data', '[{ "id":"1", "title": "Current Location", "description": "Search by GPS Location" }]')
        .attr('data-list', 'wpfbSearchList')
        .attr('data-search-in', 'title')
        .attr('data-value-property', 'id')
        .attr('data-visible-properties', '["title", "description"]')
        .attr('value', '')
		.flexdatalist({
            minLength: 0,
            noResultsText: '',
            searchIn: 'title'
        });
    $('.searchform #s, .et_pb_searchform .et_pb_s').on('change:flexdatalist', function(event, set, options) {
        wpfb.searchForm = $(event.currentTarget).closest('form')[0];
        if (set.value === '1' && navigator.geolocation) {
            navigator.geolocation.getAccurateCurrentPosition(
                wpfb.geoSuccess,
                wpfb.geoError,
                wpfb.geoProgress,
                { 
                    desiredAccuracy: 20,
                    maxWait: 15000 
                }
            );
            wpfb.isFindingLocation = true;
        }
    });
})(jQuery);

<?php get_header(); ?>

<?php

    // From https://www.geodatasource.com/developers/php
    function distance($lat1, $lon1, $lat2, $lon2, $unit='') {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
      
        if ($unit == "K") {
          return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
          } else {
              return $miles;
            }
    }

	if ( !empty( $_GET['wpfbLocation'] ) ) {
		// Move to get_location function in wpfb_shop_meta or utility function file
		$location_json = urldecode( stripslashes( $_GET['wpfbLocation'] ) );
		$location = json_decode($location_json);
		$location->accuracy = filter_var( $location->accuracy, FILTER_SANITIZE_NUMBER_INT );
		$location->latitude = filter_var( $location->latitude, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
		$location->longitude = filter_var( $location->longitude, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
		$latlng = $location->latitude .','. $location->longitude;
		$geo_request = file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?latlng='. $latlng );
		$geo_result = json_decode($geo_request);
		$search_address = $geo_result->results[0]->formatted_address;
		$search_latitude = $geo_result->results[0]->geometry->location->lat;
		$search_longitude = $geo_result->results[0]->geometry->location->lng;
	}
?>
<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
		<?php
			$shops_found = false;
			$shops_map_shortcode = '[et_pb_map address_lat="'. $search_latitude .'" address_lng="'. $search_longitude .'" address="'. $search_address .'" zoom_level="15"]';
			if (have_posts()) : while (have_posts()) : the_post();
				if ($post->post_type === 'wpfb_shop') {
					$shops_found = true;
					$post_meta = get_post_meta($post->ID);
					$shop_title = get_the_title();
					$shop_address = !empty( $post_meta['shop_address'][0]) ? $post_meta['shop_address'][0] : '';
					ob_start();
					the_content();
					$shop_content = ob_get_clean();
					$shop_latitude = !empty( $post_meta['shop_latitude'][0]) ? $post_meta['shop_latitude'][0] : '';
					$shop_longitude = !empty( $post_meta['shop_longitude'][0]) ? $post_meta['shop_longitude'][0] : '';

					$shop_distance = distance($location->latitude, $location->longitude, $shop_latitude, $shop_longitude);

					if ($shop_distance < 2) {
						$shop_pin = '[et_pb_map_pin zoom_level="18" title="'. $shop_title;
						$shop_pin .= '" pin_address="'. $shop_address;
						$shop_pin .= '" pin_address_lat="'. $shop_latitude;
						$shop_pin .= '" pin_address_lng="'. $shop_longitude;
						$shop_pin .= '"]'. $shop_content .'[/et_pb_map_pin]';
						$shops_map_shortcode .= $shop_pin;
					}
				}
				endwhile; 
				$shops_map_shortcode .= '[/et_pb_map]';
			endif;

			if ($shops_found) {
				?>
				<h3 class="map-title">Map Results</h3>
				<article <?php post_class( 'et_pb_post' ); ?>>
					<?php echo do_shortcode( $shops_map_shortcode ); ?>
				</article><?php
			}
		?>
		<?php
			rewind_posts();

			if ( have_posts() ) :
		?><h3>Search Results</h3><?php
				while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format();
		?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>

				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_pb_post_main_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					et_divi_post_format_content();

					if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
						if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) :
							printf(
								'<div class="et_main_video_container">
									%1$s
								</div>',
								$first_video
							);
						elseif ( ! in_array( $post_format, array( 'gallery' ) ) && 'on' === et_get_option( 'divi_thumbnails_index', 'on' ) && '' !== $thumb ) : ?>
							<a class="entry-featured-image-url" href="<?php the_permalink(); ?>">
								<?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
							</a>
					<?php
						elseif ( 'gallery' === $post_format ) :
							et_pb_gallery_images();
						endif;
					} ?>

				<?php if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) : ?>
					<?php if ( ! in_array( $post_format, array( 'link', 'audio' ) ) ) : ?>
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php endif; ?>

					<?php
						et_divi_post_meta();

						if ( 'on' !== et_get_option( 'divi_blog_style', 'false' ) || ( is_search() && ( 'on' === get_post_meta( get_the_ID(), '_et_pb_use_builder', true ) ) ) ) {
							truncate_post( 270 );
						} else {
							the_content();
						}
					?>
				<?php endif; ?>

					</article> <!-- .et_pb_post -->
			<?php
					endwhile;

					if ( function_exists( 'wp_pagenavi' ) )
						wp_pagenavi();
					else
						get_template_part( 'includes/navigation', 'index' );
				else :
					get_template_part( 'includes/no-results', 'index' );
				endif;
			?>
			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php

get_footer();

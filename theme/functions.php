<?php

define('WPFB_POST_TYPES', array( 'wpfb_drink', 'wpfb_shop'));

add_action( 'wp_enqueue_scripts', 'my_enqueue_assets' ); 

function my_enqueue_assets() { 
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' ); 
} 

function emp_customize_register($wp_customize) {
	$wp_customize->add_control( 'et_divi[footer_copr_text]', array(
		'label'		=> __( 'Enter Copyright Text', 'Divi' ),
		'section'	=> 'et_divi_footer_elements',
		'type'      => 'text',
	) );
}
add_action( 'customize_register', 'emp_customize_register', 11 );

function wpfb_et_builder_post_types( $post_types ) {
    $post_types = array_merge($post_types, WPFB_POST_TYPES);
     
    return $post_types;
}
add_filter( 'et_builder_post_types', 'wpfb_et_builder_post_types' );

wp_register_style( 'flex_data_list_css', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-flexdatalist/2.2.4/jquery.flexdatalist.min.css' );
wp_register_script( 'flex_data_list_js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-flexdatalist/2.2.4/jquery.flexdatalist.min.js', array( 'jquery' ), null, true );
wp_register_script( 'wpfb_geo_accurate', plugin_dir_url('') . 'wpfb/theme/geoAccurate.js', array( 'jquery', 'flex_data_list_js' ), null, true );
wp_register_script( 'wpfb_custom_search', plugin_dir_url('') . 'wpfb/theme/customSearch.js', array( 'jquery', 'flex_data_list_js', 'wpfb_geo_accurate' ), null, true );

function enqueue_custom_search() {
	global $post;
	if ( has_shortcode($post->post_content, 'et_pb_search' )) {
		wp_enqueue_style( 'flex_data_list_css' );
		wp_enqueue_script( 'flex_data_list_js' );
		wp_enqueue_script( 'wpfb_geo_accurate' );
		wp_enqueue_script( 'wpfb_custom_search' );
	}
}
add_action('wp_enqueue_scripts', 'enqueue_custom_search');

?>

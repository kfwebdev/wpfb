<?php

/**
 * This class contains the custom post type methods.
 */

class WPFB_Drink_Post_Type {
    function __construct() {
        $this->register_drink();
    }

    private function register_drink() {
        $args = array(
            'label'              => __( 'Drinks' ),
            'description'        => __( 'Drink' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'd' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'show_in_rest'       => true,
            'rest_base'          => 'wpfb_drink',
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'comments', 'page-attributes' )
        );

        register_post_type( 'wpfb_drink', $args );
    }
}

new WPFB_Drink_Post_Type();
